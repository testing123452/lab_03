public class Application
{
	public static void main(String[] args)
	{
		Student jp = new Student();
		jp.name = "Einstein";
		jp.subject = "Math";
		jp.studentID = 1;
		
		Student john = new Student();
		john.name = "John";
		john.subject = "Science";
		john.studentID = 2;
		
		System.out.println(john.name + ", " + john.subject + ", " + john.studentID);
		System.out.println(jp.name + ", " + jp.subject + ", " + jp.studentID);
		
		jp.studentID();
    jp.yourSubj();
		john.studentID();
    john.yourSubj();
		
		Student[] section4 = new Student[3];
		
		section4[0] = jp;
		section4[1] = john;
		
		section4[2] = new Student();
    section4[2].name = "Bella";
    section4[2].subject = "History";
    section4[2].studentID = 3;
    
    System.out.println(section4[0].name + ", " + section4[0].subject + ", " + section4[0].studentID);
    System.out.println(section4[1].name + ", " + section4[1].subject + ", " + section4[1].studentID);
    System.out.println(section4[2].name + ", " + section4[2].subject + ", " + section4[2].studentID);
    
    section4[2].studentID();
    section4[2].yourSubj();
	}
}